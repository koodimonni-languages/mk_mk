��    Z      �     �      �     �     �     �     �  
   �     �          	               %     .     <     A     I     R     ^  
   j     u     �     �     �     �  
   �  
   �     �     �     �  	   �  	   �     �     �     �     �     �     �     	     	     	      	     2	     H	     `	     d	     j	     s	  
   y	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
      
     <
     E
     J
     ]
     f
     m
     r
  
   v
     �
     �
     �
  O   �
     �
     �
     �
       
        #  
   (     3     :     U     b     w     �     �     �     �     �  3  �  "        %     2     I     V  
   i     t     }     �     �     �  $   �  
   �     �       !        ;     D     U     k     ~     �     �     �     �     �     �     �     �     �                     )  &   8     _     {     �     �     �  
   �     �  
   �     �          '     0     H  ,   Y     �     �  #   �     �     �     �  
             7  
   L  
   W     b     q     �     �     �     �  
   �     �     �     �       �        �     �     �     �     �               .  ;   ;     w     �     �     �     �     �  %   �                     !      X   "          @       K          Q   -          U   E   /   	                   T   Y                           P   :   +   6       )       N   #   V       Z          B   C      (   W   ,          '   H   ?       <           J             >       F   9   %                     
              =             ;   I         S           7      G      8   R   0   A                   4   5   1   M       L       D   $       *   .   &   2              3   O        Add to cart Address Admin menu nameOrders Cancel Categories Checkout Page City Country Coupon code Coupons Customer Customer note Date Default Defaults Delete note Description Dimensions Display type Docs Edit Email Emails First Name First name Guest In stock Item Last Name Last name N/A Name None Order Order Notes Order Total Order by Orders Out of stock Page settingCart Page settingCheckout Page settingMy Account Pay Phone Postcode Price Processing Product Product categories Products Qty Refunded Reviews Same as parent Save changes Search Select a country&hellip; Settings Settings group labelCheckout Settings tab labelCheckout Shipping Show Specific Countries Standard Status Tags Tax Tax Status Tax statusNone Taxable Tel: This is a demo store for testing purposes &mdash; no orders shall be fulfilled. Total Totals Username Variation #%s of %s Variations View View Order Weight WooCommerce Recent Reviews ZIP/Postcode default-slugproduct default-slugshop out of 5 product slugproduct slugproduct-category slugproduct-tag Project-Id-Version: WooCommerce
Report-Msgid-Bugs-To: https://github.com/woothemes/woocommerce/issues
POT-Creation-Date: 2014-12-16 14:43:08+00:00
PO-Revision-Date: 2014-12-11 17:47+0000
Last-Translator: Claudio Sanches <contato@claudiosmweb.com>
Language-Team: Macedonian (Macedonia) (http://www.transifex.com/projects/p/woocommerce/language/mk_MK/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: mk_MK
Plural-Forms: nplurals=2; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : 1;
X-Generator: grunt-wp-i18n 0.4.9
 Додади во кошничка Адреса Подредувања Откажи Категории Плати Град Држава Шифра на купон Купони Клиент Забелешка на клиент Датум Стандардно Стандардно Избриши забелешка Опис Димензии Прикажи вид Документи Измени е-маил е-маил Име Име  Гостин На залиха Производ Презиме Презиме Недостапно Име Празно Нарачај Забелешки на нарачка Вкупна нарачка Подреди по Подредувања Нема на залиха Кошничка Плати Моја сметка Плати Телефон Поштенски код Цена Се процесира Производ Категорија на производи Производи Кол. Повратени средства Рецензии Исто како главна Зачувај измени Барај Избери држава: Нагодувања Плати Плати Достава Прикажи Одредени држави Стандардно Статус Ознаки Данок ДДВ статус Празно Оданочено Телефон: Ова е демо продавница за тестирањеЧ ниедна нарачка нема да се исполни  Вкупно Вкупно Корисничко име Варијација #% од % Варијации Погледни Види нарачка Тежина Скорешни коментари на производи Поштенски код производ продавница од 5 производ производ производ-категорија производ-ознака 